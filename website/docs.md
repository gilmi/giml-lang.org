---
template = "website.mustache"
header = "Giml Docs"
name = "Documents"
order = 3
---

## Haddocks Index

The Giml implementation is separated into the following packages:

- [giml-syntax](haddocks/giml-syntax-0.1.0.0/index.html) - AST, parser and prettyprinter of the Giml language.
- [giml-transform](haddocks/giml-transform-0.1.0.0/index.html) - Typing and transformations for Giml.
- [giml-compiler](haddocks/giml-compiler-0.1.0.0/index.html) - A compiler from Giml to JavaScript.
- [giml-backend-js](haddocks/giml-backend-js-0.1.0.0/index.html) - Definition and backend of a subset of JavaScript used as a compilation target for Giml.

## Other documents

- [Code examples](https://gitlab.com/gilmi/giml-lang/-/tree/main/examples)
- [Language reference](https://gitlab.com/gilmi/giml-lang/-/blob/main/language-ref.org)
- [Implementation overview](https://gitlab.com/gilmi/giml-lang/-/blob/main/overview.org)
- [Wish list](https://gitlab.com/gilmi/giml-lang/-/blob/main/wishlist.org)
