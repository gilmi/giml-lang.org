---
template = "website.mustache"
header = "The Giml Project"
name = "The Project"
order = 2
---

## The Project

Giml is an open source hobby project developed by [Gil Mizrahi](https://gilmi.me) and is licensed under [Apache-2.0](https://gitlab.com/gilmi/giml-lang/-/blob/main/LICENSE).

Giml (formerly known as Strema) started as as a fun 'Live coding a compiler' streaming project and most of the work has been developed live on Twitch.

Giml is still very much a work-in-progress and many critical features are currently missing, but feel free to play around with it if you'd like!

## Why?

Giml (pronounced [Gimel](https://en.wikipedia.org/wiki/Gimel#Hebrew_gimel)) was born from my desire to:

1. Have fun making a programming language
2. Add structural typing features to a Haskell-like language
3. Experiment with a language that does not provide complex type-level programming features

In terms of language complexity, Giml sits somewhere between Elm and PureScript,
providing higher-kinded types but does not provide typeclasses and other type-level programming features.

My hope is that by streaming the development process of Giml
I can help make Haskell and building compilers a bit more approachable.

## Source & Docs

Giml's [source repository](https://gitlab.com/gilmi/giml-lang) and [issue tracker](https://gitlab.com/gilmi/giml-lang/issues) are hosted on Gitlab.

Giml is currently pre-0.1 release and is still changing rapidly.
To play with Giml-HEAD, see the build instructions in the [README](https://gitlab.com/gilmi/giml-lang/-/blob/main/README.md).

Various documentation including haddocks, language reference and examples can be found in [the docs page](docs.html).

## The Development

If you're interested in watching Giml's developement videos, check out the
[Youtube playlist](https://www.youtube.com/watch?list=PLhMOOgDOKD4IkQM75GkAnXI-fpIrDAnsu&v=khAKcFgziWU).

## Resources

If you are interested in Haskell or compilers development but don't know where to start, check out the following resources:

- [Learn Haskell by building a blog generator](https://learn-haskell.blog)
- [Reading Simple Haskell](https://soupi.github.io/rfc/reading_simple_haskell/)
- [Fun compilers](https://github.com/soupi/rfc/blob/master/fun-compilers.md)
