---
template = "website.mustache"
header = "A strict, purely functional programming language"
name = "Home"
order = 1
---

<section class="center">
	<iframe width="640" height="360" src="https://www.youtube-nocookie.com/embed/videoseries?list=PLhMOOgDOKD4IkQM75GkAnXI-fpIrDAnsu" frameborder="0" allow="encrypted-media; picture-in-picture" allowfullscreen></iframe>
</section>

**Giml** is a strict, statically typed, purely functional **programming language** with emphasis on structural typing **developed live on stream**. See the [project page](project.html) for more information.

<section class="side-by-side">

<div class="example">

<script>
var hideGiml = function() {
    document.getElementById("giml-code-example").style.display = "none";
    document.getElementById("js-code-example").style.display = "block";
};
var hideJs = function() {
    document.getElementById("js-code-example").style.display = "none";
    document.getElementById("giml-code-example").style.display = "block";
};
</script>

<h2>
Example Code
<button onclick="hideJs()">Giml</button>
<button onclick="hideGiml()">JavaScript</button>
</h2>

<div id="giml-code-example">

```haskell hljs
-- linked list data type and map operation

main = ffi( "console.log", head (map (add 1) list) )

List a =
	| Nil
	| Cons { head : a, tail : List a }

list =
	Cons { head: 1, tail: Cons { head: 2, tail: Nil } }

head list =
	case list of
		| Cons { head: head } ->
			head

map f xs =
	case xs of
		| Nil -> Nil
		| Cons { head: x, tail: rest } ->
			Cons { head: f x, tail: map f rest }
```

</div>

<div id="js-code-example" style="display: none">

```javascript hljs
"use strict";
var map = function(f) {
  return function(xs) {
    return (function(_case_0) {
        if ("Nil" === _case_0._constr) {
          return (function() {
              return {
                "_constr": "Nil",
                "_field": {}
              };
            })
            ();
        }
        if ("Cons" === _case_0._constr && true && true) {
          return (function(x, rest) {
              return (function(_data) {
                  return {
                    "_constr": "Cons",
                    "_field": _data
                  };
                })
                ({
                  "head": f(x),
                  "tail": map(f)(rest)
                });
            })
            (_case_0._field.head, _case_0._field.tail);
        }
      })
      (xs);
  };
};
var list = (function(_data) {
    return {
      "_constr": "Cons",
      "_field": _data
    };
  })
  ({
    "head": 1,
    "tail": (function(_data) {
        return {
          "_constr": "Cons",
          "_field": _data
        };
      })
      ({
        "head": 2,
        "tail": {
          "_constr": "Nil",
          "_field": {}
        }
      })
  });
var head = function(list) {
  return (function(_case_1) {
      if ("Cons" === _case_1._constr && true) {
        return (function(head) {
          return head;
        })(_case_1._field.head);
      }
    })
    (list);
};
var main = function() {
  return console.log(head(map((function(x) {
      return function(y) {
        return x + y;
      };
    })(1))
    (list)
  ));
};
main();
```

</div>

</div>

<div class="features">

## Features

- ✓ First-class Functions
- ✓ Algebraic Data Types
- ✓ Pattern Matching
- ✓ Extensible Records
- ✓ Polymorphic Variants
- ✓ Higher Kinded Types
- ✓ Purely Functional
- ✓ Strict Evaluation
- ✓ Static Typing & Type Inference
- ○ Written in Haskell
- ○ Targets JavaScript

More info [here](https://gitlab.com/gilmi/giml-lang/-/blob/main/language-ref.org).

</div>

</section>
