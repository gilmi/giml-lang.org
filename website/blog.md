---
template = "website.mustache"
header = "Giml News"
name = "Blog"
order = 4
---

- 2023/09/30 - [Implementing kind inference](https://gilmi.me/blog/post/2023/09/30/kind-inference)
- 2021/04/13 - [Typing polymorphic variants in Giml](https://gilmi.me/blog/post/2021/04/13/giml-typing-polymorphic-variants)
- 2021/04/10 - [Typing extensible records in Giml](https://gilmi.me/blog/post/2021/04/10/giml-typing-records)
- 2021/04/06 - [Giml's type inference engine](https://gilmi.me/blog/post/2021/04/06/giml-type-inference)
- 2021/03/13 - [Strema is now Giml (Giml announcement)](https://gilmi.me/blog/post/2021/03/13/giml-announcement)
- 2021/02/21 - [Live coding a compiler (Strema announcement)](https://gilmi.me/blog/post/2021/02/11/strema-streaming)
